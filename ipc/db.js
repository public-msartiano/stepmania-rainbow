const sqlite3 = require('sqlite3').verbose();
const Database = require('sqlite-async');

// new sqlite3.Database('./stepmania-rainbow.sqlite3');

/*
    is_zip // true/false
    zip_size // 15419846
    path_inzip_song // amplitude\komputerkontroller // used only if is_zip === true

    path_song // C:\a.zip\amplitude\komputerkontroller
    path_pack // C:\a.zip or C:\AMPLITUDE_PACK === zips_analysed.zip_path
    song_parsed_info // true/false
    song_pack_name // a.zip or AMPLITUDE_PACK
    song_file_title // komputerkontroller (from the file)
    song_meta_title // Komputerkontroller (from metafile)
    song_meta_artist // RobotRockerz (from metafile)
    song_meta_difficulties // 1, 3, 11, 20 (csv)
    song_metaused_type // sm
    song_metaused_source // file.sm
    song_meta_files // JSON.parse({ sm: ['x', 'y'], ...})

    playlist_ids // 1, 4, 2 (csv)
*/
const creationStatements = [
    `PRAGMA journal_mode = OFF;`,
    `PRAGMA page_size = 65536;`,
    `CREATE TABLE IF NOT EXISTS songs (id INTEGER PRIMARY KEY AUTOINCREMENT, 
        is_zip                  INTEGER DEFAULT 0,
        zip_size                INTEGER DEFAULT 0,
        path_inzip_song         TEXT,

        path_pack               TEXT,
        path_song               TEXT,
        song_parsed_info        INTEGER DEFAULT 0,
        song_pack_name          TEXT,
        song_file_title         TEXT,
        song_meta_title         TEXT,
        song_meta_artist        TEXT,
        song_meta_difficulties  TEXT,
        song_metaused_type      TEXT,
        song_metaused_source    TEXT,
        song_meta_files         TEXT,

        playlist_ids            TEXT,
        UNIQUE(path_song)
    );`,
    `CREATE TABLE IF NOT EXISTS zips_analysed (id INTEGER PRIMARY KEY AUTOINCREMENT, 
        zip_path                TEXT,
        zip_size                INTEGER,
        success                 INTEGER
    );`,
    `CREATE TABLE IF NOT EXISTS options (id INTEGER PRIMARY KEY AUTOINCREMENT, 
        folder_sm_songs         TEXT DEFAULT "",
        folder_packs            TEXT DEFAULT "",
        folder_extract_dir      TEXT DEFAULT ""
    );`,
    `CREATE TABLE IF NOT EXISTS playlists (id INTEGER PRIMARY KEY AUTOINCREMENT, 
        name                    TEXT,
        color_bg                TEXT DEFAULT "rgb(0, 0, 0)",
        color_fg                TEXT DEFAULT "rgb(255, 255, 255)",
        UNIQUE(id, name)
    );`,
    // `CREATE TABLE IF NOT EXISTS options (id INTEGER PRIMARY KEY AUTOINCREMENT, )`
];
const insertionStatements = [
    `INSERT OR IGNORE INTO options (id) values(1);`,
    `INSERT OR IGNORE INTO playlists (id, name) values(1, "generated");`,
];


let db;
Database.open('./stepmania-rainbow.sqlite3').then(async _db => {
    db = _db;
    await db.transaction(db => {
        return Promise.all([
            ...creationStatements.map(s => db.run(s))
        ]);
    });
    await db.transaction(db => {
        return Promise.all([
            ...insertionStatements.map(s => db.run(s))
        ]);
    });
});

const _createSong = async song => {
    const result = await db.run(`INSERT INTO songs (
        is_zip,
        zip_size,
        path_inzip_song,
        path_song,
        path_pack,
        song_parsed_info,
        song_pack_name,
        song_file_title,
        song_meta_title,
        song_meta_artist,
        song_meta_difficulties,
        song_metaused_type,
        song_metaused_source,
        song_meta_files
    ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, [
        song.is_zip,
        song.zip_size,
        song.path_inzip_song,
        song.path_song,
        song.path_pack,
        song.song_parsed_info,
        song.song_pack_name,
        song.song_file_title,
        song.song_meta_title,
        song.song_meta_artist,
        song.song_meta_difficulties.join(', '),
        song.song_metaused_type,
        song.song_metaused_source,
        JSON.stringify(song.song_meta_files),
    ]);
    console.log(`DB - Song ${result.lastID} created! | ${song.song_meta_artist} - ${song.song_meta_title}`);
}

const _updateSong = async (song, id) => {
    const result = await db.run(`UPDATE songs SET 
        is_zip=?,
        zip_size=?,
        path_inzip_song=?,
        path_song=?,
        path_pack=?,
        song_parsed_info=?,
        song_pack_name=?,
        song_file_title=?,
        song_meta_title=?,
        song_meta_artist=?,
        song_meta_difficulties=?,
        song_metaused_type=?,
        song_metaused_source=?,
        song_meta_files=?
        WHERE id=?`, [
        song.is_zip,
        song.zip_size,
        song.path_inzip_song,
        song.path_song,
        song.path_pack,
        song.song_parsed_info,
        song.song_pack_name,
        song.song_file_title,
        song.song_meta_title,
        song.song_meta_artist,
        song.song_meta_difficulties.join(', '),
        song.song_metaused_type,
        song.song_metaused_source,
        JSON.stringify(song.song_meta_files),
        id
    ]);
    console.log(`DB - Song ${id} UPDATED! | ${song.song_meta_artist} - ${song.song_meta_title}`);
}


const generateSingleTransaction = song => {
    const tQuery = `INSERT OR REPLACE INTO songs (
        is_zip,
        zip_size,
        path_inzip_song,
        path_song,
        path_pack,
        song_parsed_info,
        song_pack_name,
        song_file_title,
        song_meta_title,
        song_meta_artist,
        song_meta_difficulties,
        song_metaused_type,
        song_metaused_source,
        song_meta_files
    ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);`;
    const tParameters = [
        song.is_zip,
        song.zip_size,
        song.path_inzip_song,
        song.path_song,
        song.path_pack,
        song.song_parsed_info,
        song.song_pack_name,
        song.song_file_title,
        song.song_meta_title,
        song.song_meta_artist,
        song.song_meta_difficulties.join(', '),
        song.song_metaused_type,
        song.song_metaused_source,
        JSON.stringify(song.song_meta_files),
    ];
    return [tQuery, tParameters];
}

const updateSongPlaylist = (song, playlists) => {
    const result = db.run(`UPDATE songs SET 
        playlist_ids=?
        WHERE id=?`, [
        playlists.join(', '),
        song.id
    ]);
    return result.changes === 1;
}

const addSongs = async songs => {
    /*
    let pars = [];
    songs.forEach(song => {
        const [tQuery, tPars] = generateSingleTransaction(song);
        // pars = [...pars, ...tPars];
        pars.push(tPars);
    });

    await db.run("BEGIN");
    songs.forEach(async song => {
        const [tQuery, tPars] = generateSingleTransaction(song);
        
        await db.run(tQuery, tPars);
    });
    await db.run("COMMIT;");
    */

    /*
    songs.forEach(async song => {
        const [tQuery, tPars] = generateSingleTransaction(song);
        await db.run(`INSERT OR REPLACE INTO songs (
            is_zip,
            path_inzip_song,
            path_song,
            path_pack,
            song_parsed_info,
            song_pack_name,
            song_file_title,
            song_meta_title,
            song_meta_artist,
            song_meta_difficulties,
            song_metaused_type,
            song_metaused_source,
            song_meta_files
        ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);`, tPars);
    });*/

    /*
    const result = db.transaction(db => {
        return Promise.all([
            ...songs.map(song => {
                const [tQuery, tPars] = generateSingleTransaction(song);
                return db.exec(tQuery, tPars)
                    .catch(err => console.log('inerr', err));
            })
        ]);
    })
        .then()
        .catch(err => console.log('err: ', err));

    console.log(`DB - AddSongs - modified ${songs.length} rows`);

    return result;*/
    
    songs.forEach(async song => {
        const [tQuery, tPars] = generateSingleTransaction(song);
        await db.run(tQuery, tPars);
    });

    console.log(`DB - AddSongs - modified ${songs.length} rows`);
}

const addSong = async song => {
    return db.get('SELECT id from songs WHERE path_song=?', [song.path_song])
        .then(row => {
            if (row && row.id) {
                _updateSong(song, row.id);
            } else {
                _createSong(song);
            }
        })
        .catch(err => console.log('ERROR while adding song'));
}
const getSongsCount = async () => (await db.get(`SELECT count(id) as count FROM songs;`)).count;
const getSongs = async searchFilters => {
    let filters = [];
    if (searchFilters) {
        /*
        songsFilters: {
                searchText: 'linkin',
                oneDiffOf: 6,
        }
        */
        if (searchFilters.searchText.length > 1) {
            filters.push(`(
                song_pack_name LIKE "%${searchFilters.searchText}%" OR
                song_meta_artist LIKE "%${searchFilters.searchText}%" OR
                song_meta_title LIKE "%${searchFilters.searchText}%" OR
                song_file_title LIKE "%${searchFilters.searchText}%"
            )`);
        }
    }

    let where = '';
    if (filters.length > 0) where = ' WHERE '+filters.join(' ');

    let order = ' ORDER BY song_meta_artist ASC, song_meta_title ASC, song_file_title ASC, song_pack_name ASC ';

    const query = `SELECT * FROM songs ${where} ${order};`;

    let songs = [];
    try {
        await db.each(query, function(err, song) {
            song.song_meta_difficulties = song.song_meta_difficulties.split(', ');
            song.song_meta_files = JSON.parse(song.song_meta_files);
            songs.push(song);
        });
    } catch (exception) {}

    return {
        count: await getSongsCount(),
        songs
    }
}

const setOptionsFolderSmSongs = async folderChosen => {
    return await db.run(`UPDATE options set folder_sm_songs=? where ID=1`, [folderChosen]);
}
const setOptionsFolderPacks = async folderChosen => {
    return await db.run(`UPDATE options set folder_packs=? where ID=1`, [folderChosen]);
}
const setOptionsExtractionFolder = async folderChosen => {
    return await db.run(`UPDATE options set folder_extract_dir=? where ID=1`, [folderChosen]);
}
const getOptions = async () => await db.get(`SELECT * from options where ID=1;`);

const getPlaylists = async () => await db.all(`SELECT * from playlists;`);

const removeAllSongsForPackPath = async path_pack => await db.run(`DELETE FROM songs WHERE path_pack=?;`, [path_pack]);
const zipWasAnalysed = async (zip_path, zip_size) => {
    try {
        const row = await db.get(`SELECT * from zips_analysed where zip_path=?;`, [zip_path]);
        if (
            (row && row.id) &&
            row.success
        ) {
            if (row.zip_size !== zip_size) { // if zip size is different, drop previous songs registered for this zip, and reanalyse later
                const result = await removeAllSongsForPackPath(zip_path); // { lastID: 0, changes: 5 }
                console.log(`DB - Zip songs cleaned: ${result.changes} | ${zip_path} (${zip_size})`);
                return false;
            }
            return true;
        }
    } catch (exc) { return true; }

    return false;
}

const zipUpdate = async (zip_path, zip_size, success) => {
    try {
        const row = await db.get(`SELECT * from zips_analysed where zip_path=?`, [zip_path]);
        if (row && row.id && row.success) {
            return db.run(`UPDATE zips_analysed set zip_path=?, zip_size=?, success=? where ID=?`, [zip_path, zip_size, success, row.id]);
        }
    } catch (exc) {}

    db.run(`INSERT INTO zips_analysed (zip_path, zip_size, success) VALUES(?, ?, ?);`, [zip_path, zip_size, success]);
}

module.exports = {
    db: {
        addSong,
        addSongs,
        getSongs,
        updateSongPlaylist,
        getOptions,
        setOptionsFolderSmSongs,
        setOptionsFolderPacks,
        setOptionsExtractionFolder,
        getPlaylists,
        zipWasAnalysed,
        zipUpdate,
    },
};
