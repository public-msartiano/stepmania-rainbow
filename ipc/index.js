const { ipcMain, shell, dialog } = require('electron');
const filewalker = require('filewalker');
const { parser, songsPaths, getMetadataForType, getMainMetaFileZipPath, songsByPack, resetSongsMetaData } = require('./songMetadataParser');
const StreamZip = require('node-stream-zip');
const nodePath = require('path');
const nodeFs = require('fs');
const { db } = require('./db');
const { mainWindow } = require('../main');
const { debounce, sleep, sendLog } = require('./utils');

let lastSearchTerm = 'linkin';
const dispatchSongs = async (event, passedSearchFilters) => {
    let searchFilters = {
        searchText: lastSearchTerm
    };
    if (passedSearchFilters) {
        searchFilters = { ...passedSearchFilters };
        lastSearchTerm = passedSearchFilters.searchText;
    }

    const data = await db.getSongs(searchFilters);
    sendLog(event, `Dispatching [${data.songs.length}/${data.count}] songs to Frontend`);
    event.reply("API_RESPONSE_GET_SONGS", data);
}

const completeAnalysis = () => {
    resetSongsMetaData();
    filesFound = 0;
    filesProcessed = 0;
    zipFilesFound = 0;
    zipFilesProcessed = 0;
    zipFilesCurrentlyOpen = 0;
    subZipFilesFound = 0;
    subZipFilesProcessed = 0;
}

let processingFiles = false;
let updateSent = false;
let filesFound = 0;
let filesProcessed = 0;
let individualZipFilesFound = 0;
let individualZipFilesProcessed = 0;
let zipFilesFound = 0;
let zipFilesProcessed = 0;
let zipFilesCurrentlyOpen = 0;
const addProcessedFile = event => {
    filesProcessed++;
    sendLog(event, `processed/tot : ${filesProcessed}/${filesFound}`);
    // sendLog(event, `processed/tot : ${filesProcessed}/${filesFound} - processingFiles: ${processingFiles}`);
    if (filesFound === filesProcessed && filesFound > 0 && !processingFiles) { // if this is the last file, do all the analysis
        sendLog(event, "Starting Zip processing ...");
        zipFiles = Object.values(songsPaths).filter(s => s.is_zip === true);
        individualZipFilesFound = zipFiles.length;
        individualZipFilesProcessed = 0;

        // const songsByPack = getSongsByPack(songsPaths);

        const ks = Object.keys(songsByPack());
        zipFilesFound = ks.length;
        zipFilesProcessed = 0;
        zipFilesCurrentlyOpen = 0;
        // [{tutti a.zip}, {tutti b.zip}, {etc}]
        ks.forEach(async k => {
            while (zipFilesCurrentlyOpen >= 20) {
                debounce(() => sendLog(event, 'Zip - Lock applied, >=100 parallel reads'), 100, true);
                await sleep(100);
            }
            debounce(() => sendLog(event, 'Zip - Lock removed'), 100, true);

            const songsInPack = songsByPack()[k];

            sendLog(event, 'Zip - ready to handle zip '+k);
            if (songsInPack.length > 0) {
                const zipWasAnalysed = await db.zipWasAnalysed(songsInPack[0].path_pack, songsInPack[0].zip_size);
                if (zipWasAnalysed) {
                    return sendLog(event, 'Zip - Already analysed');
                }
                handleZipPack(event, songsInPack);
            }
        });

            /*
        songsByPack.forEach(songsInPackObject => {
            const ks = Object.keys(songsInPackObject);
            ks.forEach(k => {
                const songsInPack = songsInPackObject[k];
                handleZipPack(event, songsInPack);
            });
        });*/

        /*
        zipFiles.forEach(async song => {
            while (zipFilesCurrentlyOpen >= 50) {
                debounce(() => console.log('Zip - Lock applied, >=100 parallel reads'), 2000, true);
                await sleep(2000);
            }
            debounce(() => console.log('Zip - Lock removed'), 2000, true);
            handleZip(event, song);
        });*/
        sendLog(event, "Finished Zip processing.");
    }
}

const handleZipPack = (event, songs) => {
    zipFilesCurrentlyOpen++;

    if (songs.length < 1) {
        zipFilesProcessed++;
        zipFilesCurrentlyOpen--;
        return;
    }

    const zipLocation = songs[0].path_pack;
    const zip = new StreamZip({ file: zipLocation, storeEntries: true });
    zip.on('error', err => {
        sendLog(event, `INDIVIDUAL ZIP ERROR - ${song.path_pack} - ${err}`);
        zipFilesProcessed++;
        zipFilesCurrentlyOpen--;
        zip.close();
        if (!updateSent && zipFilesProcessed === zipFilesFound) {
            completeAnalysis();
            updateSent = true;
            dispatchSongs(event);
            sendLog(event, `Individual Zips: ${individualZipFilesProcessed}/${individualZipFilesFound}`);
        }
    });
    zip.on('ready', async () => {
        zipFilesProcessed++;
        let subZipFilesProcessed = 0;
        const subZipFilesFound = songs.length;

        // for each song, do things
        songs.forEach(async (song, i) => {
            individualZipFilesProcessed += 1;
            subZipFilesProcessed++;

            const [metaZipPath, metaZipType] = getMainMetaFileZipPath(song);
            if (!metaZipPath || !metaZipPath) { subZipFilesProcessed++; return; }

            const data = zip.entryDataSync(metaZipPath).toString('utf8');
            const metadata = getMetadataForType(metaZipType, data, metaZipPath);

            sendLog(event, `Zip: ${zipFilesProcessed}/${zipFilesFound} | ${subZipFilesProcessed}/${subZipFilesFound} | (${song.song_pack_name}) ${metadata.song_meta_artist} - ${metadata.song_meta_title}`);
            
            songs[i] = { ...song, ...metadata };
            // songsPaths[song.path_song] = { ...songsPaths[song.path_song], ...metadata };
            // await db.addSong(songsPaths[song.path_song]);
            // await db.addSong(song);
        });

        // insert all to db
        await db.addSongs(songs);
        await db.zipUpdate(songs[0].path_pack, songs[0].zip_size, true);

        zipFilesCurrentlyOpen--;
        zip.close();

        if (!updateSent && zipFilesProcessed === zipFilesFound) {
            completeAnalysis();
            updateSent = true;
            dispatchSongs(event);
            sendLog(event, `Individual Zips: ${individualZipFilesProcessed}/${individualZipFilesFound}`);
        }
    });
}

const handleZip = (event, song) => {
    zipFilesCurrentlyOpen++;
    sendLog(event, 'Zip deflate: '+song.path_pack);

    const [metaZipPath, metaZipType] = getMainMetaFileZipPath(song);
    if (!metaZipPath || !metaZipPath) { zipFilesProcessed++; return; }

    const zip = new StreamZip({ file: song.path_pack, storeEntries: true });
    zip.on('error', err => {
        sendLog(event, `INDIVIDUAL ZIP ERROR - ${song.path_pack} - ${err}`);
        zipFilesProcessed++;
        zipFilesCurrentlyOpen--;
        if (!updateSent && zipFilesProcessed === zipFilesFound) {
            updateSent = true;
            dispatchSongs(event);
        }
    });
    zip.on('ready', async () => {
        zipFilesProcessed++;

        const data = zip.entryDataSync(metaZipPath).toString('utf8');
        zip.close();
        zipFilesCurrentlyOpen--;

        const metadata = getMetadataForType(metaZipType, data, metaZipPath);

        sendLog(event, `Zip: ${zipFilesProcessed}/${zipFilesFound} | (${song.song_pack_name}) ${metadata.song_meta_artist} - ${metadata.song_meta_title}`);
        songsPaths[song.path_song] = { ...songsPaths[song.path_song], ...metadata };
        await db.addSong(songsPaths[song.path_song]);

        if (!updateSent && zipFilesProcessed === zipFilesFound) {
            updateSent = true;
            dispatchSongs(event);
        }
    });
}

ipcMain.on('ANALYSE_DIR', async (event) => {
    processingFiles = true;
    updateSent = false;
    filesFound = 0;
    filesProcessed = 0;

    let { folder_packs } = await db.getOptions();
    if (folder_packs.length < 1) return;

    folder_packs = folder_packs + nodePath.sep;

    filewalker(folder_packs)
        .on('dir', function(p) {
            // console.log('dir:  %s', p);
        })
        .on('file', function(p, s) {
            filesFound++;
            // console.log('file: %s, %d bytes', p, s.size);
            parser({ path: p, meta: s, fullPath: folder_packs + p }, () => addProcessedFile(event), event);
        })
        .on('error', function(err) {
            console.error(err);
        })
        .on('done', function() {
            sendLog(event, `${this.dirs} dirs, ${this.files} files, ${this.bytes} bytes`);
            // sendLog(event, 'numExt' + numExt);
            processingFiles = false;
        })
        .walk();
});

ipcMain.on('DEBUG_SONGS', (event) => {
    event.reply('SONGLIST_RECEIVED', songsPaths);
});

ipcMain.on('API_GET_SONGS', async (event, searchFilters) => dispatchSongs(event, searchFilters));
ipcMain.on('API_GET_OPTIONS', async event => {
    const options = await db.getOptions();
    event.reply("API_RESPONSE_GET_OPTIONS", options);
});
ipcMain.on('CHOOSE_OPTIONS_FOLDER_SM_SONGS', async (event, currentDir) => {
    const options = { properties: ['openDirectory'] };
    if (currentDir && currentDir.length > 1) options.defaultPath = currentDir;
    const result = await dialog.showOpenDialog(mainWindow, { ...options });
    if (result.filePaths.length > 0) {
        await db.setOptionsFolderSmSongs(result.filePaths[0]);
        event.reply("API_RESPONSE_GET_OPTIONS", await db.getOptions());
    }
});
ipcMain.on('CHOOSE_OPTIONS_FOLDER_PACKS', async (event, currentDir) => {
    const options = { properties: ['openDirectory'] };
    if (currentDir && currentDir.length > 1) options.defaultPath = currentDir;
    const result = await dialog.showOpenDialog(mainWindow, { ...options });
    if (result.filePaths.length > 0) {
        await db.setOptionsFolderPacks(result.filePaths[0]);
        event.reply("API_RESPONSE_GET_OPTIONS", await db.getOptions());
    }
});
ipcMain.on('CHOOSE_OPTIONS_EXTRACTION_FOLDER', async (event, extractionFolder) => {    
    await db.setOptionsExtractionFolder(extractionFolder);
    event.reply("API_RESPONSE_GET_OPTIONS", await db.getOptions());
});

ipcMain.on('API_GET_PLAYLISTS', async (event) => event.reply("API_RESPONSE_GET_PLAYLISTS", await db.getPlaylists()));


let extractedSongs = 0;
let extractedTotSongs = 0;
ipcMain.on('EXTRACT_SONGS', async (event, songs, smSongsFolder, extractionFolder, playlist) => {
    const outputFolder = smSongsFolder + nodePath.sep + extractionFolder;
    extractedTotSongs = songs.length;
    extractedSongs = 0;
    songs.forEach(song => extractSong(event, song, outputFolder, playlist));
});

const extractSong = async (event, song, outputPath, playlist) => {
    // const pathWithPlaylist = outputPath + nodePath.sep + playlist.name + nodePath.sep;
    const pathWithPlaylist = outputPath + nodePath.sep;
    
    if (!nodeFs.existsSync(pathWithPlaylist)){
        nodeFs.mkdirSync(pathWithPlaylist);
    }
    sendLog(event, `Extracting [${song.path_pack}]/[${song.path_inzip_song}] to ${pathWithPlaylist} | ${song.song_meta_artist} - ${song.song_meta_title}`);

    const zip = new StreamZip({ file: song.path_pack, storeEntries: true });
    zip.on('error', err => {
        sendLog(event, `EXTRACT ZIP ERROR - ${song.path_pack} - ${err}`);
        extractedSongs++;
        if (extractedSongs === extractedTotSongs) dispatchSongs(event);
    });
    zip.on('ready', async () => {
        const path_inzip_with_separator_right = song.path_inzip_song.split('\\').join('/');
        const destination_dir = `${pathWithPlaylist}${getSongNiceName(song)}${nodePath.sep}`;
        
        if (!nodeFs.existsSync(destination_dir)) {
            nodeFs.mkdirSync(destination_dir);
        }
        zip.extract(path_inzip_with_separator_right, destination_dir, async err => {
            sendLog(event, `Extracting ${song.path_inzip_song}`);
            sendLog(event, err ? 'Extract error' : 'Extracted');

            await db.updateSongPlaylist(song, [1]);
            zip.close();

            extractedSongs++;
            if (extractedSongs === extractedTotSongs) dispatchSongs(event);
        });

        /*
        zip.extract(song.path_inzip_song, pathWithPlaylistName, async err => {
            console.log(err ? 'Extract error' : 'Extracted');

            await db.updateSongPlaylist(song, [1]);
            zip.close();

            extractedSongs++;
            if (extractedSongs === extractedTotSongs) dispatchSongs(event);
        });*/
    });
}

ipcMain.on('OPEN_URL', (_, url) => shell.openExternal(url));

getSongNiceName = song => {
    const song_file_title = song.song_file_title.replace(/[^a-z0-9]/gi, '_');
    const song_meta_title = song.song_meta_title.replace(/[^a-z0-9]/gi, '_');
    const song_meta_artist = song.song_meta_artist.replace(/[^a-z0-9]/gi, '_');
    const song_pack_name = song.song_pack_name.replace(/[^a-z0-9]/gi, '_');

    let songDirName = song_file_title;
    if (song_meta_title.length > 1) songDirName = song_meta_title;
    if (song_meta_artist.length > 1) songDirName = `${song_meta_artist} - ${songDirName}`;
    if (song_pack_name.length > 1) songDirName = `${songDirName} [${song_pack_name}]`;

    //str = str.replace(/💚|💛|🧡|❤️|( 📼)|(📼)|( 🕹️)|(🕹️)/gi, '');
    songDirName = songDirName.replace(/([\uE000-\uF8FF]|\uD83C[\uDF00-\uDFFF]|\uD83D[\uDC00-\uDDFF])/g, '');
    
    return songDirName;
}