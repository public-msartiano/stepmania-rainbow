const nodePath = require('path');
const StreamZip = require('node-stream-zip');
const { db } = require('./db');
const { sendLog } = require('./utils');

// archives
const archivesExtensions = ['zip', 'rar'];
// this is what we're looking for
const metaExtensions = ['sm', 'dwi', 'ssc', 'ksf', 'sma', 'xstep', 'ds', 'dsx', 'bms', 'aup', 'ksh', 'ffr', 'adwi'];
// binary/proprietary forms of metadata, useless if not able to read them
const metaBinaryExtensions = ['dw', 'stx', 'bme']; // bme is readible created by RAW2BMS9th, but usally bms is the output
// Song lyric/subtitles
const lyricExtensions = ['lrc', 'srt'];
// check if an audio file is present in this folder! otherwise we dont count it
const audioExtensions = ['ogg', 'mp3', 'wav', 'osz', 'wma']; // .mp3-
// check if banner is present for further metadata
const imageExtensions = ['jpg', 'jpeg', 'png', 'bmp', 'tif']; //banner.psp .pdn .bga
// check if video is present for further metadata
const videoExtensions = ['avi', 'mpg', 'mpeg', 'mp4', 'm4v', 'mov', 'wmv', 'mkv', 'ogm', 'webm'];
// useless
const variousExtensions = [
    'xml', 'ini', 'sprite', 'old', 'lua', 'db', 'edit',
    'txt', 'redir', 'model', 'actor', 'older', 'xls',
    'alt', 'html', 'xlsx', 'ods', 'ds_store', 'gif',
    '--temp--', 'psd', '', 'sfl', 'tmx', 'tsx', 'md',
    'rtf', 'log', 'scenechange', 'asd', 'ogg___', 'tm1', // dwi.tm1
    'gme', 'mc', 'psm', 'mcd', // all of these are effects files it seems
    's', '124124', 'sfk', 'xcf', 'rde', 'rds', 'nsd', 'mxm',
    'crs', // extra info about packs (?)
    'tm4+', 'extra', 'pdf', 'url', 'old-alt', 'doc',
    'txt~', 'preview', 'v1old', 'v1', 'v1older', 'oldalt',
    'bak', 'oldest', 'older', 'def', 'bpm', 'mif',
    'smzip', '!sm', 'new', 'sgp', 'smexp', '1st', 'emdaer',
    'me', 'diz', 'auto', 'frag', 'swf', 'wpk',
    'originalbanner', '!jpg', 'sdt', 'mcs', 'part',
    'dwi~', 'tm3', 'tm4', 's3m', 'sm~', 'sm2', 'tm2', 'sm_', 'swp', 'dwiold', 'smold', 'dwi old', 'sm old', 'c__dwi', 'c__sm', // backup files
    'jbf', 'osu', 'py', 'gay1', 'fuckyou', 'long',
    '1', '2', '3', 'rb', 'backup', 'nope', 'no',
    'exe', 'wps', 'copy', 'ats', 'au', 'tmp', 'raw', 'lnk',
    'pix', 'dat', 'frm', 'ok', 'mid', 'htm', 'psp', 'pdn',
    'old2', 'pk', 'sh', 'scn', 'reapeaks', 'xmp', 'nwc',
    'xsl', 'svg', 'docx', '404', 'flv', 'js', 'css', 'notblue',
    'c', 'random', 'oxps', 'ico', 'vert', 'vdi', 'moon',
    'bga', 'bat', 'nfo', 'vbs', 'fla', 'jpg1', 'ptm',
    'info', 'skn', 'orig', 'hq', 'chart', 'ion', 'sps',
    'psx', 'mcb', 'png1', 'cfg', 'spp', 'lst', 'm2r', 'base',
    'ddrsf', 'oldest1', 'oldest2', 'mp3-', 'xmldummy',
    'really', 'oldsync', 'include', 'csv', 'fixed', 'ogm',
    'dpr', 'cache', 'uas', 'pcx', 'mwr', 'ips', 'gen', 'dib',
    'obj', 'alb', 'mtl', 'ldf', 'dll', 'pyc', 'ms3d', 
];
const excludedExtensions = [
    // ...metaExtensions,
    ...metaBinaryExtensions,
    ...archivesExtensions,
    ...lyricExtensions,
    ...variousExtensions,
    ...imageExtensions,
    ...audioExtensions,
    ...videoExtensions
];

const getExt = path => {
    let ext = nodePath.extname(path).toLowerCase();
    if (ext[0] === '.') ext = ext.substr(1);
    return ext
}

const getMetadataForType = (type, data, metaFilename) => {
    const metadata = {
        song_meta_title: '',
        song_meta_artist: '',
        song_meta_difficulties: [],
        song_metaused_type: type,
        song_metaused_source: metaFilename,
        song_parsed_info: true,
    };
    
    if (type === 'dwi') {
        let titleMatch = /#TITLE:(.+?)(?:;?)(?:\n|\r\n)/igm.exec(data);
        if (titleMatch && titleMatch.length > 1) metadata.song_meta_title = titleMatch[1];
        let artistMatch = /#ARTIST:(.+?)(?:;?)(?:\n|\r\n)/igm.exec(data);
        if (artistMatch && artistMatch.length > 1) metadata.song_meta_artist = artistMatch[1];

        for (const match of data.matchAll(/#SINGLE:(?:.+?):(.+?):?(?::|;|(?:\n|\r\n))/igm)) {
            if (match && match.length > 1) {
                let difficulty = match[1];
                if (!isNaN(difficulty)) difficulty = parseInt(difficulty, 10);
                metadata.song_meta_difficulties.push(difficulty);
            }
        }
    }
    else if (type === 'sm') {
        let titleMatch = /#TITLE:(.+?)(?:;?)(?:\n|\r\n)/igm.exec(data);
        if (titleMatch && titleMatch.length > 1) metadata.song_meta_title = titleMatch[1];
        let artistMatch = /#ARTIST:(.+?)(?:;?)(?:\n|\r\n)/igm.exec(data);
        if (artistMatch && artistMatch.length > 1) metadata.song_meta_artist = artistMatch[1];

        //for (const match of data.matchAll(/#NOTES:(?:\n|\r\n).+?(?:\n|\r\n).+?(?:\n|\r\n).+?(?:\n|\r\n)     (\d+):(?:\n|\r\n)/igm)) {
        for (const match of data.matchAll(/#NOTES\d?:(?:\n|\r\n).+?(?:\n|\r\n).+?(?:\n|\r\n).+?(?:\n|\r\n).+?(\d+):(?:\n|\r\n)/igm)) {
            if (match && match.length > 1) {
                let difficulty = match[1];
                if (!isNaN(difficulty)) difficulty = parseInt(difficulty, 10);
                metadata.song_meta_difficulties.push(difficulty);
            }
        }
    }
    metadata.song_meta_difficulties = [...new Set(metadata.song_meta_difficulties)].sort((a, b) => a - b);
    return metadata;
};

const getMainMetaFileZipPath = song => {
    const result = [false, false];
    if ('sm' in song.song_meta_files && song.song_meta_files.sm.length > 0) {
        result[0] = song.song_meta_files.sm[0];
        result[1] = 'sm';
    } else if ('dwi' in song.song_meta_files && song.song_meta_files.dwi.length > 0) {
        result[0] = song.song_meta_files.dwi[0];
        result[1] = 'dwi';
    }

    return result;
}

const handleZipFile = async (fullPath, meta, addProcessedFile, event) => {
    const zipWasAnalysed = await db.zipWasAnalysed(fullPath, meta.size);
    if (zipWasAnalysed) {
        sendLog(event, `Zip already analysed: ${fullPath} (${meta.size})`)
        return addProcessedFile(event);
    }

    const zip = new StreamZip({
        file: fullPath,
        storeEntries: true
    });
    zip.on('error', err => {
        sendLog(event, `ZIP ERROR - ${fullPath} - ${err}`);
        addProcessedFile(event);
        db.zipUpdate(fullPath, meta.size, false);
        zip.close();
    });
    zip.on('ready', () => {
        // console.log('Entries read: ' + zip.entriesCount);
        for (const entry of Object.values(zip.entries())) {
            const desc = entry.isDirectory ? 'directory' : `${entry.size} bytes`;
            // console.log(`Entry ${entry.name}: ${desc}`);
            const subExt = getExt(entry.name);
            
            entry.size = meta.size;

            if (!entry.isDirectory && !archivesExtensions.includes(subExt)) parser({ path: entry.name, meta: entry, fullPath: `${fullPath}${nodePath.sep}${entry.name}`, isZip: true });
        }
        // console.log('numExt', numExt);

        // Do not forget to close the file once you're done
        // console.log('Entries read: ' + zip.entriesCount);
        // console.log(fullPath);
        addProcessedFile(event);
        // db.zipUpdate(fullPath, meta.size, true);
        zip.close();
    });
}

const numExt = {};
let songsPaths = {};
let songsByPack = {};

/*
meta: {
  dev: 3135845508,
  mode: 33206,
  nlink: 1,
  uid: 0,
  gid: 0,
  rdev: 0,
  blksize: 4096,
  ino: 281474977662587,
  size: 90717604,
  blocks: 177184,
  atimeMs: 1588803499355.993,
  mtimeMs: 1563504768000,
  ctimeMs: 1588897578968.8657,
  birthtimeMs: 1588803498542.1675,
  atime: 2020-05-06T22:18:19.356Z,
  mtime: 2019-07-19T02:52:48.000Z,
  ctime: 2020-05-08T00:26:18.969Z,
  birthtime: 2020-05-06T22:18:18.542Z
}
*/
const parser = async ({ path, meta, fullPath, isZip = false }, addProcessedFile, event) => {
    let ext = getExt(path);
    
    let extNormalised = ext.length == 1 ? ext + ' ' : ext;
    extNormalised = ext.length == 2 ? ext + ' ' : ext;
    
    if (ext == 'zip') return await handleZipFile(fullPath, meta, addProcessedFile, event); // fullPath here is C:\a.zip and equals song.path_pack
    if (!isZip) return addProcessedFile(event); // for now only zip is allowed!

    if (excludedExtensions.includes(ext)) return;

    if (!excludedExtensions.includes(ext) && !metaExtensions.includes(ext)) {
        return sendLog(event, `[${extNormalised}] !!!!!!!! @@@@@@@@ UNKNOWN EXTENSION @@@@@@@@ !!!!!!!! ${isZip?'**zip**/':''}${fullPath}`);
    }

    // console.log(`[${extNormalised}] ${isZip?'**zip**/':''}${fullPath}`);

    if (!(ext in numExt)) numExt[ext] = 1;
    else { numExt[ext] += 1; }

    const parentFolder = nodePath.normalize(nodePath.dirname(fullPath));
    const baseFolder = fullPath.substr(0, fullPath.indexOf(path));

    const path_song = parentFolder;
    const path_pack = nodePath.dirname(baseFolder) + nodePath.sep + nodePath.basename(baseFolder);
    const defaultSong = {
        is_zip:                 isZip,
        zip_size:               meta.size,
        path_inzip_song:        nodePath.normalize(nodePath.dirname(path)), // amplitude\komputerkontroller used only if is_zip

        path_song:              path_song, // C:\a.zip\amplitude\komputerkontroller
        path_pack:              path_pack, // C:\a.zip or C:\AMPLITUDE_PACK
        song_parsed_info:       false,
        song_pack_name:         nodePath.basename(baseFolder), // a.zip or AMPLITUDE_PACK
        song_file_title:        nodePath.basename(parentFolder), // komputerkontroller (from the file)
        song_meta_title:        '',
        song_meta_artist:       '',
        song_meta_difficulties: [],
        song_metaused_type:     '',
        song_metaused_source:   '',
        song_meta_files:        {},
    };

    if (!(parentFolder in songsPaths)) {
        songsPaths[parentFolder] = { ...defaultSong };
    }
    if (!songsPaths[parentFolder]['song_meta_files'][ext]) {
        songsPaths[parentFolder]['song_meta_files'][ext] = [];
    }
    songsPaths[parentFolder]['song_meta_files'][ext].push(path);


    if (!(path_pack in songsByPack)) {
        songsByPack[path_pack] = [];
    }
    const thatElement = songsByPack[path_pack].find(s => s.path_song === path_song);
    if (thatElement) {
        if (!thatElement['song_meta_files'][ext]) {
            thatElement['song_meta_files'][ext] = [];
        }
        thatElement['song_meta_files'][ext].push(path);
    } else {
        const thatElement = { ...defaultSong };
        thatElement['song_meta_files'][ext] = [path];
        songsByPack[path_pack].push(thatElement);
    }
}

const resetSongsMetaData = () => {
    songsPaths = {};
    songsByPack = {};
    console.log('Resetted metadata', songsPaths, songsByPack);
}

module.exports = {
    parser,
    numExt,
    songsPaths,
    getMetadataForType,
    getMainMetaFileZipPath,
    songsByPack: () => songsByPack,
    resetSongsMetaData,
};
