import React from 'react';
import HomePage from './pages/HomePage';
import { Connect } from './Store';
import { db, sortByKey } from './common/utils';

export default () => {
    const initialState = {
        songs: [],
        songsMeta: {
            count: 0,
        },
        options: {
            folder_extract_dir: '',
            folder_packs: '',
            folder_sm_songs: '',
        },
        playlists: [],
        songsFilters: {
            searchText: 'linkin',
            oneDiffOf: 6,
            selectedSongs: [],
        },
        debug: {
            logs: [],
        }
    };
    window.initialState = initialState;

    const reducer = (state = initialState, action) => {
        switch (action.type) {
            case 'SET_SELECTED_SONGS': {
                return {
                    ...state,
                    songsFilters: {
                        ...state.songsFilters,
                        selectedSongs: action.data
                    }
                };
            }
            case 'SET_SEARCH_TEXT_AND_SEARCH': {
                const filters = {
                    ...state.songsFilters,
                    searchText: action.data
                }
                window.ipcRenderer.send('API_GET_SONGS', filters);
                return {
                    ...state,
                    songsFilters: { ...filters }
                };
            }
            case 'ADD_SONG':
                if (state.songs.filter(s => s.path === action.data.path).length > 0) return state;
                state.songs.push(action.data);
                return state;
            case 'ADD_SONGS': {
                console.log("add songs!", action);
                const newState = { ...state, songs: [...state.songs] };
                for (let [key, song] of Object.entries(action.data)) {
                    newState.songs = newState.songs.filter(s => s.path_song !== song.path_song);
                    newState.songs.push(song);
                }
                console.log('newState', newState);
                return newState;
            }
            case 'API_ADD_SONGS': {
                console.log("add songs!", action);
                const newState = { ...state, songs: [...state.songs] };
                action.data.songs.forEach(song => {
                    newState.songs = newState.songs.filter(s => s.path_song !== song.path_song);
                    newState.songs.push(song);
                });
                newState.songsMeta.count = action.data.count;
                return newState;
            }
            case 'API_GET_OPTIONS': {
                console.log('options: ', action.data);
                return { ...state, options: { ... action.data } };
            }
            case 'API_GET_PLAYLISTS': {
                return { ...state, playlists: [...action.data] };
            }
            case 'DEBUG_LOG': {
                const newLogs = [...state.debug.logs];
                newLogs.push(action.data);
                if (newLogs.length > 10) newLogs.splice(0, 1);
                console.log(`DEBUG_LOG - ${newLogs.length} -  ${action.data}`);

                return {
                    ...state,
                    debug: {
                        ...state.debug,
                        logs: newLogs
                    }
                };
            }
            default:
                return state;
        }
    };

    return (
        <Connect initialState={initialState} reducer={reducer}>
            <HomePage />
        </Connect>
    );
};
