const get = (key, defaultValue) => {
    const value = localStorage.getItem(key);
    if (!value) return defaultValue;
    return JSON.parse(value);
};
const set = (key, value) => localStorage.setItem(key, JSON.stringify(value));
const remove = key => localStorage.removeItem(key);

export const getSiteKey = key => `${key}-sites`;

export const db = {
    categories: {
        get: () => get('categories', []),
        set: value => set('categories', value),
    },
    sites: {
        get: category => get(getSiteKey(category), {}),
        set: (category, value) => set(getSiteKey(category), value),
        removeByCategory: (category) => remove(getSiteKey(category))
    },
    options: {
        get: () => get('options', {}),
        set: value => set('options', value),
    }
};

export const getLowestPriceInCategory = category => {
    const sites = db.sites.get(category);

    let leastPriceSite = { prices: [9999999] };
    let currentLowestPrice = 9999999;

    sites.forEach(site => {
        site.prices.forEach(price => {
            const thisPrice = parseFloat(price.replace(/£|€|\$/gi, ''), 10);
            if (thisPrice < currentLowestPrice) {
                currentLowestPrice = thisPrice;
                leastPriceSite = site;
            }
        });
    });

    return leastPriceSite;
};

export const sortByKey = (arr, key) => arr.sort((a, b) => {
    if (a[key] < b[key]) return -1;
    if (a[key] > b[key]) return 1;
    return 0;
});

const trim = str => str.replace(/\s\s+/g, ' ').trim();

const throttleBy = 10000; // 10 seconds
let lastExecutedFn = Date.now();
export const throttle = fn => {
    const now = Date.now();
    if (now >= lastExecutedFn) lastExecutedFn = now + throttleBy;
    else lastExecutedFn += throttleBy;
    const relativeTimeFromNow = Math.abs(lastExecutedFn - now);

    setTimeout(() => {
        console.log('[throttle] fn executed!');
        fn();
    }, relativeTimeFromNow);
    console.log('[throttle] called, setting next to ', lastExecutedFn);
};
