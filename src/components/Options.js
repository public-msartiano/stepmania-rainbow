import React, { useRef } from 'react';
import { useStore } from '../Store';

export default () => {
    const [store, dispatch] = useStore();
    const refExtractDir = useRef(store.options.folder_extract_dir);

    const extractSongsToOutputFolder = () => {
        console.log('store.options.folder_sm_songs', store.options.folder_sm_songs);
        console.log('store.options.folder_extract_dir', store.options.folder_extract_dir);
        if (!store.options.folder_sm_songs || store.options.folder_sm_songs.length < 2 || store.options.folder_extract_dir.length < 2) return;

        window.ipcRenderer.send('EXTRACT_SONGS', store.songsFilters.selectedSongs, store.options.folder_sm_songs, store.options.folder_extract_dir, store.playlists[0]);
        dispatch('SET_SELECTED_SONGS', []);
    }
    const temp_folder_extract_dir = store.options.folder_extract_dir;
    
    return (
        <div>
            <hr />
            <div>
                <button onClick={() => window.ipcRenderer.send('CHOOSE_OPTIONS_EXTRACTION_FOLDER', refExtractDir.current.value)}>Save subfolder name</button>
                <input type="text" ref={refExtractDir} />
                {store.options.folder_extract_dir}
                <button className="btn-action" disabled={store.options.folder_extract_dir.length < 2 || store.options.folder_sm_songs.length < 2 ? true : false} onClick={extractSongsToOutputFolder}>Extract selection to /songs/{store.options.folder_extract_dir}</button> 
            </div>

            <hr />
            <div>
                <button onClick={() => window.ipcRenderer.send('CHOOSE_OPTIONS_FOLDER_PACKS', store.options.folder_packs)}>Choose search folder</button>
                {store.options.folder_packs}
                <button className="btn-action" disabled={store.options.folder_packs.length < 2 ? true : false} onClick={() => window.ipcRenderer.send('ANALYSE_DIR')}>Search songs</button>
            </div>

            <hr />
            <div>
                <button onClick={() => window.ipcRenderer.send('CHOOSE_OPTIONS_FOLDER_SM_SONGS', store.options.folder_sm_songs)}>Choose /songs folder</button>
                {store.options.folder_sm_songs}
            </div>

            <hr />
            <div style={{ marginBottom: '20px' }}>
                Debug
                <textarea style={{ height: '180px', width: '100%' }} disabled={true} value={store.debug.logs.join('\n')} />
            </div>

        </div>
    );
}
