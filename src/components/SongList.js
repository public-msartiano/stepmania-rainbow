import React, { useEffect, useState, useRef } from 'react';
import { useStore } from '../Store';

export default () => {
    const [store, dispatch] = useStore();
    const selectedSongs = store.songsFilters.selectedSongs;
    const setSelectedSongs = arr => dispatch('SET_SELECTED_SONGS', arr);
    const [limitMinDifficulty, setMinDifficulty] = useState(0);
    const [limitMaxDifficulty, setMaxDifficulty] = useState(99);
    const [shouldHaveAtLeastOneDiffBelow, setShouldHaveAtLeastOneDiffBelow] = useState(6);
    const refSearch = useRef(store.songsFilters.searchText);
    const refMinDifficulty = useRef(0);
    const refMaxDifficulty = useRef(99);
    const refShouldHaveAtLeastOneDiffBelow = useRef(6);

    /*
    useEffect(() => {
        const pingUrlReply = (_, { id, url, html, category }) => addSite({ id, url, html, category });
        window.ipcRenderer.on('ADD_SITE_REPLY', pingUrlReply);

        return () => {
            window.ipcRenderer.removeListener('UPDATE_SITE', pingUrlReply);
        };
    });
    */
    
    const debugSongsFound = () => {
        window.ipcRenderer.send('DEBUG_SONGS');
    }

    const limitDown = isNaN(limitMinDifficulty) ? 0 : parseInt(limitMinDifficulty, 10);
    const limitUp = isNaN(limitMaxDifficulty) ? 99 : parseInt(limitMaxDifficulty, 10);
    const shouldHaveOneDiffBelow = isNaN(shouldHaveAtLeastOneDiffBelow) ? 99 : parseInt(shouldHaveAtLeastOneDiffBelow, 10);
    const filteredSongs = store.songs.filter(s => {
        const minDiff = Math.min(...s.song_meta_difficulties);
        const maxDiff = Math.max(...s.song_meta_difficulties);
        
        const searchT = store.songsFilters.searchText;
        if (searchT.length > 0 && (
            !s.song_pack_name.toLowerCase().includes(searchT) &&
            !s.song_meta_artist.toLowerCase().includes(searchT) &&
            !s.song_meta_title.toLowerCase().includes(searchT) &&
            !s.song_file_title.toLowerCase().includes(searchT)
        )) return false;

        if (limitDown > 0 && minDiff < limitDown) return false;
        if (limitUp < 99 && maxDiff > limitUp) return false;

        if (shouldHaveOneDiffBelow < 99 && minDiff > shouldHaveOneDiffBelow) return false;

        return true;
    });

    const selectSong = (song, reactIndex) => {
        let newSelectedSongs = [...selectedSongs];
        if (newSelectedSongs.includes(song)) {
            newSelectedSongs.splice(newSelectedSongs.indexOf(song), 1);
            console.log('removing '+song.song_meta_title);
        } else {
            newSelectedSongs.push(song);
            console.log('adding '+song.song_meta_title);
        }

        setSelectedSongs(newSelectedSongs);
    };

// <td>{JSON.stringify(s.metadata)}</td>
// <td>{JSON.stringify(s.metaFiles)}</td>
//             <button onClick={debugSongsFound}>Debug found songs</button>
// <button onClick={extractSongsToOutputFolder}>Extract selected songs</button>

    const searchSong = () => {
        dispatch('SET_SEARCH_TEXT_AND_SEARCH', refSearch.current.value.toLowerCase());
    }

    return (
        <div>
            <div>
                <input type="text" ref={refSearch} onKeyDown={e => { if (e.key === 'Enter') { searchSong(); }}} />
                <button onClick={searchSong}>Search</button> {store.songsFilters.searchText}
            </div>
            <div>
                <input type="number" ref={refMinDifficulty} onChange={() => setMinDifficulty(refMinDifficulty.current.value)} />
                Difficulty minimum {limitMinDifficulty}
            </div>
            <div>
                <input type="number" ref={refMaxDifficulty} onChange={() => setMaxDifficulty(refMaxDifficulty.current.value)} />
                Difficulty maximum {limitMaxDifficulty}
            </div>
            <div>
                <input type="number" ref={refShouldHaveAtLeastOneDiffBelow} onChange={() => setShouldHaveAtLeastOneDiffBelow(refShouldHaveAtLeastOneDiffBelow.current.value)} />
                Should have at least one Difficulty below.. {shouldHaveAtLeastOneDiffBelow}
            </div>
            <div>
                Songs: {filteredSongs.length}/{store.songsMeta.count}
                <hr />
            </div>
            
            <table>
                <tr>
                    <th>Pack Name</th>
                    <th>Artist</th>
                    <th>Title</th>
                    <th>File title</th>
                    <th>Diff.</th>
                </tr>
                {filteredSongs.map((s, index) => {
                    const songClasses = [];
                    if (selectedSongs.includes(s)) songClasses.push('selected');
                    if (s.playlist_ids) songClasses.push('in-playlist');
                    return (
                        <tr id={index} onClick={() => selectSong(s, index)} className={songClasses.join(' ')}>
                            <td>{s.song_pack_name}</td>
                            <td>{s.song_meta_artist || ''}</td>
                            <td>{s.song_meta_title || ''}</td>
                            <td>{s.song_file_title || ''}</td>
                            <td>{s.song_meta_difficulties.join(', ')}</td>
                        </tr>
                    );
                })}
            </table>
        </div>
    );
}
