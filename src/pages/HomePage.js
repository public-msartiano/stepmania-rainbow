import React, { useEffect, useState } from 'react';
import SongList from '../components/SongList';
import Options from '../components/Options';
import { useStore } from '../Store';
const electron = window.require("electron");

export default () => {
    const [store, dispatch] = useStore();
    const [initialised, setInitialised] = useState(false);

    useEffect(() => {
        const songlistReceived = (_, songsReceived) => dispatch('ADD_SONGS', songsReceived);
        electron.ipcRenderer.on('SONGLIST_RECEIVED', songlistReceived);
        const apiGetSongs = (_, songsReceived) => dispatch('API_ADD_SONGS', songsReceived);
        electron.ipcRenderer.on('API_RESPONSE_GET_SONGS', apiGetSongs);
        const apiGetOptions = (_, optionsReceived) => dispatch('API_GET_OPTIONS', optionsReceived);
        electron.ipcRenderer.on('API_RESPONSE_GET_OPTIONS', apiGetOptions);
        const apiGetPlaylists = (_, optionsReceived) => dispatch('API_GET_PLAYLISTS', optionsReceived);
        electron.ipcRenderer.on('API_RESPONSE_GET_PLAYLISTS', apiGetPlaylists);
        const apiDebugLog = (_, optionsReceived) => dispatch('DEBUG_LOG', optionsReceived);
        electron.ipcRenderer.on('DEBUG_LOG', apiDebugLog);

        return () => {
            electron.ipcRenderer.removeListener('SONGLIST_RECEIVED', songlistReceived);
            electron.ipcRenderer.removeListener('API_RESPONSE_GET_SONGS', apiGetSongs);
            electron.ipcRenderer.removeListener('API_RESPONSE_GET_OPTIONS', apiGetOptions);
            electron.ipcRenderer.removeListener('API_RESPONSE_GET_PLAYLISTS', apiGetPlaylists);
            electron.ipcRenderer.removeListener('DEBUG_LOG', apiDebugLog);
        };
    });

    if (!initialised) {
        console.log("send API_INIT !")
        electron.ipcRenderer.send('API_GET_SONGS', store.songsFilters);
        electron.ipcRenderer.send('API_GET_OPTIONS');
        electron.ipcRenderer.send('API_GET_PLAYLISTS');
        setInitialised(true);
    }

    return (
        <main>
            <SongList />
            <Options />
        </main>
    );
}